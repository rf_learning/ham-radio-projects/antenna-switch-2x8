# Antenna switch 2x8

2 to 8 antenna switch for ham radio, expected to work up to 500MHz




## Project stage

**Preliminary Hardware Designing**

## Project status

* [ ] Minimal component schematics.
* [ ] Minimal component schematics review.
* [ ] Board component schematics.
* [ ] Board component schematics review.
* [ ] Component selection.
* [ ] Component selection alternative.
* [ ] Footprint association.
* [ ] Footprint review.
* [ ] PCB Rules setup.
* [ ] PCB Rules setup review.
* [ ] Component placing.
* [ ] Component placing review.
* [ ] PCB rougth routing.
* [ ] PCB fine routing. 
* [ ] PCB layout review. 
* [ ] PCB DRC. 
* [ ] Gerbers. 
* [ ] Gerbers review.
* [ ] Hardware production.
* [ ] Hardware tested.
